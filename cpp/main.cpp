#include <iostream>

void print(const char** message)
{
    std::cout << *message << std::endl;
}

int main()
{
    const char* message = "Hello World!";
    print(&message);
    return 0;
}
